#include <iostream>
#include <vector>
#include <utility>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <array>
#include <iomanip>
#include <limits>

using namespace std;

#define GREEN   "\x1b[32m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

typedef const float(*FunctionPtr)(const pair<float, float>&, const pair<float, float>&);

//Euclidean distance
const float Euclidean(const pair<float, float>& node1, const pair<float, float>& node2)
{
    return sqrt(pow((node1.first - node2.first), 2) + pow((node1.second - node2.second), 2));
}

//Manhattan distance
const float Manhattan(const pair<float, float>& node1, const pair<float, float>& node2)
{
    return fabs(node1.first - node2.first) + fabs(node1.second - node2.second);
}

// returns the n closest nodes
const vector<pair<pair<float, float>, int> > neigh_nodes(const pair<float, float>& point, const int& neighbors, vector<pair<pair<float, float>, int> >& node, FunctionPtr dist_func)
{
	vector<pair<pair<float, float>, int> > result;
	float distance[neighbors], cur_dist;
	for (int i = 0; i < neighbors; ++i)
		result.push_back(node[i]), distance[i] = dist_func(point, node[i].first);
	bool find_worst = true;
	int worst_in;
	for (int i = neighbors; i < node.size(); ++i)
	{
		cur_dist = dist_func(point, node[i].first);
		if (find_worst)
		{
			worst_in = 0;
			for (int j = 1; j < result.size(); ++j)
				if (distance[j] > distance[worst_in])
					worst_in = j;
			find_worst = false;
		}
		if (cur_dist < distance[worst_in])
			result[worst_in] = node[i], distance[worst_in] = cur_dist, find_worst = true;
	}
	return result;
}

// calculates optimal point's value (lower = better)
const float value(FunctionPtr dist, const pair<float, float>& new_point, const vector<pair<pair<float, float>, int> >& points)
{
    int weight = 0;
    float distance = 0;
    for(int i = 0; i < points.size(); i++)
        weight += points[i].second, distance += dist(new_point, points[i].first);
    return (distance / weight);
}

const bool compare(const pair<pair<float, float>, float>& p0, const pair<pair<float, float>, float>& p1)
{
	return p0.second < p1.second;
}

// returns potential optimal points
const array<pair<pair<float, float>, float>, 5> potential_optimal_points(const float& rows, const float& cols, const int& neighbors, vector<pair<pair<float, float>, int> >& node, FunctionPtr dist)
{
	vector<pair<float, float> > processed;
	for (const auto& mem : node)
		processed.push_back(mem.first);
	array<pair<pair<float, float>, float>, 5> result;
	for (auto& mem : result)
		mem.second = numeric_limits<float>::max();
	pair<pair<float, float>, float> cur;
	vector<pair<pair<float, float>, int> > neigh;
	int worst_in;
	bool find_worst = true;
	for (auto& mem : node)
	{
		for(float i = 0.5; i < mem.second; i += 0.5)
		{
			// top left
			cur.first.first = mem.first.first - i, cur.first.second = mem.first.second + i;
			if (cur.first.first >= 0 && cur.first.first <= cols && cur.first.second >= 0 && cur.first.second <= rows 
			   && find(processed.begin(), processed.end(), cur.first) == processed.end())
			{
				processed.push_back(cur.first);
				neigh = neigh_nodes(cur.first, neighbors, node, dist);
				cur.second = value(dist, cur.first, neigh);
				if (find_worst)
				{
					worst_in = 0;
					for (int j = 1; j < result.size(); ++j)
						if (result[worst_in].second < result[j].second)
							worst_in = j;
					find_worst = false;
				}
				if (cur.second < result[worst_in].second)
					result[worst_in] = cur, find_worst = true;
			}
			// bottom right
			cur.first.first = mem.first.first + i, cur.first.second = mem.first.second - i;
			if (cur.first.first >= 0 && cur.first.first <= cols && cur.first.second >= 0 && cur.first.second <= rows 
			    && find(processed.begin(), processed.end(), cur.first) == processed.end())
			{
				processed.push_back(cur.first);
				neigh = neigh_nodes(cur.first, neighbors, node, dist);
				cur.second = value(dist, cur.first, neigh);
				if (find_worst)
				{
					worst_in = 0;
					for (int j = 1; j < result.size(); ++j)
						if (result[worst_in].second < result[j].second)
							worst_in = j;
					find_worst = false;
				}
				if (cur.second < result[worst_in].second)
					result[worst_in] = cur, find_worst = true;
			}
		}
	}
	sort(result.begin(), result.end(), compare);
	return result;
}

// returns the highest profit point
const pair<pair<float, float>, float> max_profit(const array<pair<pair<float, float>, float>, 5>& potential, const float cd_ratio[])
{
	pair<pair<float, float>, float> result;
	float cur_fac;
	result.first.first = potential[0].first.first, result.first.second = potential[0].first.second, result.second = potential[0].second * cd_ratio[0];
	for (int i = 1; i < potential.size(); ++i)
	{
		cur_fac = potential[i].second * cd_ratio[i];
		if (cur_fac < result.second)
			result.first.first = potential[i].first.first, result.first.second = potential[i].first.second, result.second = cur_fac;
	}
	return result;
}

int main()
{
	float rows, cols;
	cout << "input grid size: [number of rows] [number of columns] >>> ";
	cin >> rows >> cols;
	int node_cnt;
	cout << "input node count >>> ";
	cin >> node_cnt;
	vector<pair<pair<float, float>, int> > node;
	pair<pair<float, float>, int> cur;
	for (int i = 1; i <= node_cnt; ++i)
		cin >> cur.first.first >> cur.first.second >> cur.second, node.push_back(cur);
	int neighbors;
	cout << "input the number of neighboring nodes to be processed (recommended: 3 - 5) >>> ";
	cin >> neighbors;
	int demand;
	float cost;
	float cd_ratio[5];
	cout << "input coffee demand and construction cost of the five most optimal starbucks locations: [demand] [cost] >>> ";
	for (int i = 0; i < 5; ++i)
		cin >> demand >> cost, cd_ratio[i] = cost / demand;
	cout << "\n\n" << MAGENTA << setw(19) << "Manhattan distance" << setw(23) << "Euclidean distance" << endl;
	cout << CYAN << "---------------------|----------------------\n" << RESET;
	cout << left << setw(7) << "x" << setw(7) << "y" << setw(7) << "value";
	cout << right << CYAN << "|" << RESET << setw(7) << "x" << setw(7) << "y" << setw(8) << "value" << endl;
	cout << CYAN << "---------------------|----------------------\n" << RESET;
	array<pair<pair<float, float>, float>, 5> potential[2];
	potential[0] = potential_optimal_points(rows, cols, neighbors, node, &Euclidean);
	potential[1] = potential_optimal_points(rows, cols, neighbors, node, &Manhattan);
	for (int i = 0; i < 5; ++i)
		cout << left << setw(7) << fixed << setprecision(1) << setw(7) << potential[0][i].first.first
		     << setw(7) << potential[0][i].first.second << setprecision(2) << setw(7) << potential[0][i].second
		     << right << setprecision(1) << CYAN << "|" << RESET << setw(7) << potential[1][i].first.first
		     << setw(7) << potential[1][i].first.second << setprecision(2) << setw(7) << potential[1][i].second << endl;
	pair<pair<float, float>, float> mp[2];
	mp[0] = max_profit(potential[0], cd_ratio), mp[1] = max_profit(potential[1], cd_ratio);
	cout << GREEN << "\n<*> best point using Euclidean is " << fixed << setprecision(2) << mp[0].first.first
	     << ", " << mp[0].first.second << " with a profit factor of about " << mp[0].second;
	cout << "\n<*> best point using Manhattan is " << fixed << setprecision(2) << mp[1].first.first
	     << ", " << mp[1].first.second << " with a profit factor of about " << mp[1].second << endl;
}